package com.heelox.compass;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

/**
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT 
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */
public class MainActivity extends ActionBarActivity implements SensorEventListener {

    private float[] lastAccel = new float[3];
    private float[] lastMagnet = new float[3];

    private boolean lastMagnetSet = false;
    private boolean lastAccelSet = false;

    private ImageView mPointer;

    private float[] mR = new float[9];
    private float mCurrentDegree = 0f;

    private Sensor sAccelerometer;
    private Sensor sMagnetometer;
    private SensorManager sManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.mPointer = (ImageView) findViewById(R.id.mPointer);

        this.sManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);
        this.sAccelerometer = sManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        this.sMagnetometer = sManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);


    }

    @Override
    public void onPause() {
        super.onPause();
        this.sManager.unregisterListener(this, sAccelerometer);
        this.sManager.unregisterListener(this, sMagnetometer);
    }

    @Override
    public void onResume() {
        super.onResume();
        this.sManager.registerListener(this, this.sAccelerometer, SensorManager.SENSOR_DELAY_GAME);
        this.sManager.registerListener(this, this.sMagnetometer, SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor == this.sAccelerometer) {
            System.arraycopy(event.values, 0, this.lastAccel, 0, event.values.length);
            this.lastAccelSet = true;
        } else if (event.sensor == sMagnetometer) {
            System.arraycopy(event.values, 0, this.lastMagnet, 0, event.values.length);
            this.lastMagnetSet = true;
        }
        if (this.lastMagnetSet && this.lastAccelSet) {
            float[] orientation = new float[3];
            SensorManager.getRotationMatrix(mR, null, lastAccel, lastMagnet);
            SensorManager.getOrientation(mR, orientation);
            float azimuthInRadians = orientation[0];
            float azimuthInDegress = (float)(Math.toDegrees(azimuthInRadians)+360)%360;
            RotateAnimation ra = new RotateAnimation(
                    mCurrentDegree,
                    -azimuthInDegress,
                    Animation.RELATIVE_TO_SELF, 0.5f,
                    Animation.RELATIVE_TO_SELF,
                    0.5f);

            ra.setDuration(250);

            ra.setFillAfter(true);

            mPointer.startAnimation(ra);
            mCurrentDegree = -azimuthInDegress;
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // TODO Auto-generated method stub

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.about) {
            new AlertDialog.Builder(this).setTitle("About").setMessage("This application has been created by Heelox team.")
            .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {

                }
            }).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
